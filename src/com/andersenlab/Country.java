package com.andersenlab;

public class Country {
    private String countryName;
    private int countryPopulation;
    private City city;

    public Country(String countryName, int countryPopulation, City city){
        this.countryName = countryName;
        this.countryPopulation = countryPopulation;
        this.city = city;
    }

    public City getCity() {
        return city;
    }

    public void infoCountry() {
        System.out.println("Страна: " + countryName
                + ", Население, чел.: " + countryPopulation
                + ", Город: " + city);
    }

    @Override
    public String toString() {
        return "Страна: " + countryName
                + ", Население, чел.: " + countryPopulation
                + ", Город: " + city;
    }
}
