package com.andersenlab;

public class Hotel {
    private String hotelName;
    private Room room;

    public Room getRoom() {
        return room;
    }

    public Hotel(String hotelName, Room room){
        this.hotelName = hotelName;
        this.room = room;
    }

    public void infoHotel() {
        System.out.println("Отель: " + hotelName +
                " " + room);
    }

    @Override
    public String toString() {
        return "Отель: " + hotelName +
                " " + room;
    }
}
