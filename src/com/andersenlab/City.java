package com.andersenlab;

public class City {
    private String cityName;
    private Hotel hotel;

    public Hotel getHotel() {
        return hotel;
    }

    public City(String cityName, Hotel hotel){
        this.cityName = cityName;
        this.hotel = hotel;
    }

    public void infoCity() {
        System.out.println("cityName='" + cityName + '\'' +
                ", hotel=" + hotel);
    }

    @Override
    public String toString() {
        return "cityName='" + cityName + '\'' +
                ", hotel=" + hotel;
    }
}
