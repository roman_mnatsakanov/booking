package com.andersenlab;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;

public class Main {
    public static void main(String[] args) throws IOException, ParseException {

        Room firstRoom = new Room("5", "description", 4.4, 2, 500.9);
        Hotel firstHotel = new Hotel("megaHotel", firstRoom);
        Room room1 = firstHotel.getRoom();
        City firstCity = new City("megatonn", firstHotel);
        Hotel hotel = firstCity.getHotel();
        Country firstCountry = new Country("Kaanar", 10000, firstCity);
        City city = firstCountry.getCity();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        boolean exit = true;

        while(exit) {
            System.out.println("Для получения информации о стране нажмите - 1");
            System.out.println("Для получения информации о городе нажмите - 2");
            System.out.println("Для получения информации об отеле нажмите - 3");
            System.out.println("Для получения информации о комнате нажмите - 4");
            System.out.println("Для бронирования комнаты нажмите - 5");
            System.out.println("Для блокировки комнаты на даты нажмите - 6");
            String menu = reader.readLine();
            switch (menu) {
                case "1":
                    firstCountry.infoCountry();
                    break;

                case "2":
                    firstCity.infoCity();
                    break;
                case "3":
                    firstHotel.infoHotel();
                    break;
                case "4":
                    firstRoom.infoRoom();
                    break;
                case "5": {
                    System.out.println("Дата заселения: ");
                    String start = reader.readLine();
                    System.out.println("Дата выселения: ");
                    String end = reader.readLine();
                    firstRoom.dateValidation(start, end);
                    exit = false;
                    break;
                }
                default:
                    System.out.println("Подумай ещё разок");
                    break;
            }
        }
    }
}
