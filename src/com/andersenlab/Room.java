package com.andersenlab;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class Room {
    private String roomNumber;
    private String roomDescription;
    private double roomRate;
    private double roomRooms;
    private double roomPrice;

    public Room(String roomNumber, String roomDescription, double roomRate, double roomRooms, double roomPrice){
        this.roomNumber = roomNumber;
        this.roomDescription = roomDescription;
        this.roomRate = roomRate;
        this.roomRooms = roomRooms;
        this.roomPrice = roomPrice;
    }

    public void orderRoom(long resultDays) {
        double resultPrice = resultDays * roomPrice;
        System.out.println("Количество дней: " + resultDays +  "; Сумма заказа: " + resultPrice);
    }

    public void dateValidation(String start, String end) throws ParseException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate checkIn = LocalDate.parse(start, formatter);
        LocalDate checkOut = LocalDate.parse(end, formatter);
        if (checkOut.isAfter(checkIn)) {
            long resultDays = ChronoUnit.DAYS.between(checkIn, checkOut);
            orderRoom(resultDays);
        } else {
            System.out.println("Дата выселения раньше даты заселения");
        }
    }

    public void infoRoom() {
        System.out.println("Номер комнаты: " + roomNumber
                + ", Количество комнат: " + roomRooms
                + ", Стоимость: " + roomPrice
                + ", Рейтинг: " + roomRate
                + ", Описание: " + roomDescription);
    }

    @Override
    public String toString() {
        return "Номер комнаты: " + roomNumber
                + ", Количество комнат: " + roomRooms
                + ", Стоимость: " + roomPrice
                + "Рейтинг: " + roomRate
                + "Описание: " + roomDescription;
    }

}
